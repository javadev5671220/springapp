package az.ingress.firstlesson.dto;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
@Data
@Slf4j
public class Hello {
    @PostConstruct
    public void init(){
        log.info("Hello bean initialized");
    }
    @PreDestroy
    public void destroy(){
        log.info("Hello bean destroyed");
    }
}

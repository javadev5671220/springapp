package az.ingress.firstlesson.repository;

import az.ingress.firstlesson.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book,Integer> {

}

package az.ingress.firstlesson.service;

import az.ingress.firstlesson.dto.BookRequestDto;
import az.ingress.firstlesson.dto.BookResponseDto;
import az.ingress.firstlesson.model.Book;
import az.ingress.firstlesson.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BookServiceImpl implements BookService{

    private final BookRepository bookRepository;
    public BookServiceImpl(BookRepository bookRepository){
        this.bookRepository=bookRepository;
    }
    @Override
    public int create(BookRequestDto dto) {
         Book book = Book.builder()
                 .author(dto.getAuthor())
                 .name(dto.getName())
                 .countPage(dto.getCountPage())
                 .build();
         bookRepository.save(book);
         return book.getId();
    }

    @Override
    public BookResponseDto update(Integer id, BookRequestDto dto) {
        Book bookUpdate = bookRepository.findById(id)
                .orElseThrow(()->new RuntimeException("Update edəcəyiniz kitab yoxdur."));

        bookUpdate.setAuthor(dto.getAuthor());
        bookUpdate.setName(dto.getName());
        bookUpdate.setCountPage(dto.getCountPage());
        bookRepository.save(bookUpdate);

        return BookResponseDto.builder()
                .author(bookUpdate.getAuthor())
                .name(bookUpdate.getName())
                .countPage(bookUpdate.getCountPage())
                .build();
    }

    @Override
    public void delete(Integer id) {
        Optional<Book> bookOptional = bookRepository.findById(id);
        if (bookOptional.isPresent()){
            bookRepository.delete(bookOptional.get());
        }
    }

    @Override
    public BookResponseDto get(Integer id) {
        Book book = bookRepository.findById(id)
                .orElseThrow(()->new RuntimeException("Book not found."));

        return BookResponseDto.builder()
                .author(book.getAuthor())
                .name(book.getName())
                .countPage(book.getCountPage())
                .id(book.getId())
                .build();
    }
}

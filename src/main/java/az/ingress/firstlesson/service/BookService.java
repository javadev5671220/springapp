package az.ingress.firstlesson.service;

import az.ingress.firstlesson.dto.BookRequestDto;
import az.ingress.firstlesson.dto.BookResponseDto;

public interface BookService {
    int create(BookRequestDto dto);
    BookResponseDto update(Integer id,BookRequestDto dto);
    void delete(Integer id);
    BookResponseDto get(Integer id);
}

package az.ingress.firstlesson.controller;

import az.ingress.firstlesson.dto.BookRequestDto;
import az.ingress.firstlesson.dto.BookResponseDto;
import az.ingress.firstlesson.service.BookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/hello")
@Slf4j
public class HelloController {

    @GetMapping()
    public void get(){

    }
}

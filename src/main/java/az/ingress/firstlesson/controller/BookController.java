package az.ingress.firstlesson.controller;

import az.ingress.firstlesson.dto.BookRequestDto;
import az.ingress.firstlesson.dto.BookResponseDto;
import az.ingress.firstlesson.service.BookService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/book")
public class BookController {
    private final BookService bookService;
    public BookController(BookService bookService){
        this.bookService=bookService;
    }
    @GetMapping("/{id}")
    public ResponseEntity<BookResponseDto> get(@PathVariable Integer id){
        BookResponseDto response = bookService.get(id);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody BookRequestDto dto){
        int id = bookService.create(dto);
        return ResponseEntity.created(URI.create("book/" + id)).build();
    }
    @PutMapping("/{id}")
    public ResponseEntity<BookResponseDto> update(@PathVariable Integer id,
                                          @RequestBody BookRequestDto dto){
        BookResponseDto response = bookService.update(id, dto);
        return ResponseEntity.ok(response);

        // İstəsək bunlarıda göndərə bilərik
        // return ResponseEntity.ok().header("key","value").body(response);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable Integer id) {
        bookService.delete(id);
        return ResponseEntity.ok().build();
    }
}

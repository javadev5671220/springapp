package az.ingress.firstlesson;

import az.ingress.firstlesson.config.ConfigurationProp;
import jdk.jfr.Registered;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
@RequiredArgsConstructor
public class FirstlessonApplication implements CommandLineRunner {
	private final ConfigurationProp prop;
	public static void main(String[] args) {
		SpringApplication.run(FirstlessonApplication.class, args);
	}

	@Value("${ms24.lesson}")
	private Integer lessonNumber;

	@Value("${asan.url}")
	private String url;


	@Override
	public void run(String... args) throws Exception {
		log.info("ms24 lesson number is :{}", prop.getLesson());
		log.info("ms24 user list is :{}",prop.getUsers());
		log.info("Asan url: {}", url);
	}
}

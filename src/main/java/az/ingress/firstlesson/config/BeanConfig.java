package az.ingress.firstlesson.config;

import az.ingress.firstlesson.dto.Hello;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    //default olaraq @Scope("singleton") dur.
    //@Scope("prototype") bu zaman isə hər səfərində yeni bir obyekt yaradır.
    @Bean
    public Hello getHello(){
        return new Hello();
    }
}

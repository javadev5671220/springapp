package az.ingress.firstlesson.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "ms24")
@Data

public class ConfigurationProp {

    private Integer lesson;
    private List<User> users;

}
